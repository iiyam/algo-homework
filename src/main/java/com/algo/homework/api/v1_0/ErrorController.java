package com.algo.homework.api.v1_0;


import com.algo.homework.api.v1_0.exceptions.ResourceNotFoundException;
import com.algo.homework.api.v1_0.exceptions.ValidationExceptionV1_0;
import com.algo.homework.api.v1_0.responses.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
public class ErrorController {




    @ExceptionHandler(value = ValidationExceptionV1_0.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> exception(ValidationExceptionV1_0 exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getField(), exception.getReason()), HttpStatus.BAD_REQUEST);
    }





    @ExceptionHandler(value = ResourceNotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> exception(ResourceNotFoundException exception) {
        return new ResponseEntity<>(new ErrorResponse(null, exception.getMessage()), HttpStatus.NOT_FOUND);
    }





    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> exception(Exception e) {
        System.out.println(e);
        return new ResponseEntity<>(new ErrorResponse(null, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
