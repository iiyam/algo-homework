package com.algo.homework.api.v1_0.exceptions;


public class ResourceNotFoundException extends Exception{
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
