package com.algo.homework.api.v1_0;

import com.algo.homework.api.v1_0.exceptions.ResourceNotFoundException;
import com.algo.homework.api.v1_0.exceptions.ValidationExceptionV1_0;
import com.algo.homework.api.v1_0.requests.EditSomethingRequest;
import com.algo.homework.api.v1_0.responses.ErrorResponse;
import com.algo.homework.repo.model.Something;
import com.algo.homework.repo.service.SomethingMediator;
import com.wordnik.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/v1.0/api")
@Api(consumes = "application/json", produces = "application/json", description = "api return somethings objects",
        protocols = "http")
public class SomethingController {


    @Autowired
    SomethingMediator somethingService;




    @RequestMapping( value = "/somethings", method = RequestMethod.GET)
    public List<Something> downloadAllSomethingsByPaging(
            @RequestParam(value = "start", required = false) String start,
            @RequestParam(value = "size", required = false) String size) throws ValidationExceptionV1_0 {

        Integer startNumber = checkNumberValue(start, "start", 0, Integer.MAX_VALUE, 0);
        Integer sizeNumber = checkNumberValue(size, "size", 0, 20, 20);

        return this.somethingService.findSomethings(startNumber, sizeNumber);
    }




    private Integer checkNumberValue(String value, String field, int min, int max, int defaultValue) throws ValidationExceptionV1_0 {
        try{
            Integer result = null;
            if(value == null){
                result = defaultValue;
            }else{
               result = Integer.valueOf(value);
            }

            if(result < min && result > max){
                throw new Exception();
            }
            return  result;
        }catch (Exception e){
            throw  new ValidationExceptionV1_0(field, " value should be number between " + min + " " + max);
        }

    }




    private String checkStringValue(String value, String field) throws ValidationExceptionV1_0 {
        if(value == null || value.equals("")){
            throw new ValidationExceptionV1_0(field, "id param cannot be null or empty");
        }

        return  value;
    }




    @ApiOperation(value = "return Something object by id", response = Something.class, httpMethod = "GET")
    @ApiParam(name = "id", required = true, allowableValues = "not null or empty")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "bad request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "something not found", response = ErrorResponse.class),
            @ApiResponse(code = 200, message =  "found something", response = Something.class)
    })
    @RequestMapping( value = "/somethings/{id}", method = RequestMethod.GET)
    public Something downloadSomethingById(@PathVariable(value = "id") String id) throws ValidationExceptionV1_0, ResourceNotFoundException {

        id = checkStringValue(id, "id");

        return this.somethingService.findSomething(id);
    }






    @RequestMapping( value = "/somethings/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> editSomethingById(@PathVariable(value = "id") String id,
                                                @RequestBody @Valid EditSomethingRequest editSomethingRequest, BindingResult bindingResult) throws ValidationExceptionV1_0, ResourceNotFoundException {

        id = checkStringValue(id, "id");
        if(bindingResult.hasErrors()){
            List<FieldError> errors = bindingResult.getFieldErrors();
            createValidationExceptionOnErrorList(errors);
        }

        this.somethingService.editSomething(new Something(id, editSomethingRequest.getVal1(), editSomethingRequest.getVal2()));
        return new ResponseEntity<>(HttpStatus.OK);
    }



    private void createValidationExceptionOnErrorList(List<FieldError> errors) throws ValidationExceptionV1_0 {
        for(int i = 0; i< 1 && i < errors.size(); i++){ // find first error
            throw new ValidationExceptionV1_0(errors.get(i).getField(), errors.get(i).getDefaultMessage());
        }
    }



    @RequestMapping( value = "/somethings/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> deleteSomething(@PathVariable(value = "id") String id) throws ValidationExceptionV1_0, ResourceNotFoundException {
        id = checkStringValue(id, "id");
        this.somethingService.deleteSomething(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }



}
