package com.algo.homework.api.v1_0.responses;


public class ErrorResponse {
    private String info;
    private String message;


    public ErrorResponse(String info, String message) {
        this.info = info;
        this.message = message;
    }


    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
