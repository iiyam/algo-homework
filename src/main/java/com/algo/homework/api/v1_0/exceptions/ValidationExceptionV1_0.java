package com.algo.homework.api.v1_0.exceptions;


public class ValidationExceptionV1_0 extends Exception{
    private String field;
    private String reason;

    public ValidationExceptionV1_0(String field, String reason) {
        super(reason);
        this.field = field;
        this.reason = reason;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
