package com.algo.homework.api.v1_0.requests;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class EditSomethingRequest {


    @NotNull(message = "value can not be null")
    @NotEmpty(message = "value can not be empty")
    @Length(min = 3, max = 10, message = "value length must be between 3 nad 10")
    @Pattern(regexp = "[0-9]*", message = "allowed only numbers")
    private String val1;



    @NotNull(message = "value can not be null")
    @NotEmpty(message = "value can not be empty")
    @Length(min = 3, max = 30, message = "value length must be between 3 nad 30")
    @Pattern(regexp = "[a-z]*", message = "allowed only  letters from a to z ")
    private String val2;

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }


    @Override
    public String toString() {
        return "EditSomethingRequest{" +
                "val1='" + val1 + '\'' +
                ", val2='" + val2 + '\'' +
                '}';
    }
}
