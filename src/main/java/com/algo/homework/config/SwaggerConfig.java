package com.algo.homework.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@ComponentScan("com.algo.homework.api.v1_0")
public class SwaggerConfig {


    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(createApiInfo());
    }




    private ApiInfo createApiInfo(){
        return  new ApiInfo("homework", "simple rest api", "1.0",
                "http://onet.pl", "arek.czerwinski@gmail.com", "Apache Licence v. 2", "http://wp.pl");
    }
}
