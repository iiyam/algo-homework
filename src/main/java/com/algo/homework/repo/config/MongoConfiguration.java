package com.algo.homework.repo.config;


import com.algo.homework.repo.SomethingRepositoryImpl;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;

@Configuration
public class MongoConfiguration {

    private  MongoClient mongoClient;


    @Bean(name = "mongoClient")
    @Scope(value = "singleton")
    public MongoClient createMongoClient(){
        this.mongoClient = new MongoClient("localhost");
        return  this.mongoClient;
    }



    @Bean(name = "somethingRepository")
    @DependsOn(value = "mongoClient")
    public SomethingRepositoryImpl createSomethingRepository() {
        return new SomethingRepositoryImpl(mongoClient);
    }
}
