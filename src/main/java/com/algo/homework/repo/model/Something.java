package com.algo.homework.repo.model;



public class Something {
    private String id;
    private String val1;
    private String val2;

    public Something(String id, String val1, String val2) {
        this.id = id;
        this.val1 = val1;
        this.val2 = val2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    @Override
    public String toString() {
        return "Something{" +
                "id='" + id + '\'' +
                ", val1='" + val1 + '\'' +
                ", val2='" + val2 + '\'' +
                '}';
    }
}
