package com.algo.homework.repo.service;


import com.algo.homework.api.v1_0.exceptions.ResourceNotFoundException;
import com.algo.homework.repo.SomethingRepository;
import com.algo.homework.repo.model.Something;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

public class SomethingMediator {

    @Resource(name="somethingRepository")
    private SomethingRepository somethingRepository;


    public void init(){
        somethingRepository.init();
    }


    public void destroy(){
        somethingRepository.destroy();
    }


    public Integer insertAll(List<Something> list) {
        return somethingRepository.insertAll(list);
    }




    public Something findSomething(String id) throws ResourceNotFoundException {
        Something something =  somethingRepository.findSomething(id);
        if(something == null){
            throw new ResourceNotFoundException("Something object not found with id " + id);
        }

        return  something;
    }



    public void editSomething(Something something) throws ResourceNotFoundException {
        long result =  somethingRepository.editSomething(something);
        if(result == 0){
            throw new ResourceNotFoundException("Something object not found with id " + something.getId());
        }
    }



    public void deleteSomething(String id) throws ResourceNotFoundException {
        long result =  somethingRepository.deleteSomething(id);
        if(result == 0){
            throw new ResourceNotFoundException("Something object not found with id " + id);
        }

    }


    public List<Something> findSomethings(Integer start, Integer size) {
        List<Something> result = this.somethingRepository.findSomethings(start, size);
        if(result == null){
            result = new LinkedList<>();
        }

        return  result;
    }


    public SomethingRepository getSomethingRepository() {
        return somethingRepository;
    }

    public void setSomethingRepository(SomethingRepository somethingRepository) {
        this.somethingRepository = somethingRepository;
    }
}
