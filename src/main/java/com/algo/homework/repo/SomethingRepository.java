package com.algo.homework.repo;

import com.algo.homework.repo.model.Something;

import java.util.List;


public interface SomethingRepository {

     void init();

     void destroy();

     boolean createSomethingCollection();

     Integer insertAll(List<Something> list);

     Something findSomething(String id);

     long editSomething(Something something);

     long deleteSomething(String id);

     List<Something> findSomethings(Integer start, Integer size);

     boolean dropSomethingsCollection();
}
