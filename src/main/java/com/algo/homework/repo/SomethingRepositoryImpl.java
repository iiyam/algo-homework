package com.algo.homework.repo;

import com.algo.homework.repo.model.Something;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.LinkedList;
import java.util.List;


public class SomethingRepositoryImpl implements SomethingRepository{
    public static final String DATABASE_NAME = "algo";
    public static final String COLLECTION_NAME = "somethings";

    private static final String SOMETHING_ID = "id";
    private static final String SOMETHING_VAL1 = "val1";
    private static final String SOMETHING_VAL2 = "val2";


    private  MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    private MongoCollection<Document> somethingCollection;

    public SomethingRepositoryImpl(MongoClient mongoClient){
        this.mongoClient = mongoClient;
        this.mongoDatabase =  mongoClient.getDatabase(DATABASE_NAME);
    }


    @Override
    public void init(){
        //MongoCursor mongoCursor = this.mongoDatabase.listCollections().iterator();
        this.somethingCollection = this.mongoDatabase.getCollection(COLLECTION_NAME);
    }




    @Override
    public void destroy(){
        this.somethingCollection= null;
        this.mongoClient = null;
        this.mongoDatabase = null;

    }


    @Override
    public boolean createSomethingCollection() {


        if(!isExistCollectionName(COLLECTION_NAME)){
            this.mongoDatabase.createCollection(COLLECTION_NAME);
            this.somethingCollection = this.mongoDatabase.getCollection(COLLECTION_NAME);
            Bson bson = new BsonDocument("id", new BsonInt32(1));
            IndexOptions indexOptions = new IndexOptions();
            indexOptions.unique(true);
            this.somethingCollection.createIndex((bson), indexOptions);//(bson); //, new IndexOptions())

            return true;
        }

        return  false;
    }


    private boolean isExistCollectionName(String collectionName){
        MongoCursor<String> collectionNames =   this.mongoDatabase
                .listCollectionNames()
                .iterator();
        boolean isExistSomethingCollection = false;

        while(collectionNames.hasNext()){
            String name =  collectionNames.next();
            if(collectionName.equals(name)){
                isExistSomethingCollection = true;
            }
        }
        collectionNames.close();

        return isExistSomethingCollection;
    }


    @Override
    public Integer insertAll(List<Something> list) {
        List<Document> convrtedList = convertListOfSomethingsToListOfBasicDbObjects(list);
        this.somethingCollection.insertMany(convrtedList);
        return convrtedList.size();
    }



    private List<Document> convertListOfSomethingsToListOfBasicDbObjects(List<Something> list){
        List<Document> result = new LinkedList<>();
        if(list == null || list.isEmpty()){
            return result;
        }

        for(Something something : list){
            result.add(convertSomethingToBasicDBObject(something));
        }

        return result;
    }


    private Document convertSomethingToBasicDBObject(Something something){
        Document document = new Document();
        document.append(SOMETHING_ID, something.getId())
                .append(SOMETHING_VAL1, something.getVal1())
                .append(SOMETHING_VAL2, something.getVal2());

        return  document;
    }



    @Override
    public Something findSomething(String id) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("id", id);
        FindIterable<Document> result = this.somethingCollection.find(searchQuery);
        MongoCursor<Document> cursor =   result.iterator();

        Something toReturn = null;
        while (cursor.hasNext()){
            Document document = cursor.next();
            String somethingId = (String) document.get(SOMETHING_ID);
            String val1 = (String) document.get(SOMETHING_VAL1);
            String val2 = (String) document.get(SOMETHING_VAL2);

            toReturn = new Something(somethingId, val1, val2);
        }

        cursor.close();

        return toReturn;
    }


    @Override
    public long editSomething(Something something) {
        BasicDBObject newDocument = new BasicDBObject();

        BasicDBObject values = new BasicDBObject();
        values.put(SOMETHING_VAL1, something.getVal1());
        values.put(SOMETHING_VAL2, something.getVal2());
        newDocument.append("$set", values);

        return this.somethingCollection.updateOne(Filters.eq(SOMETHING_ID, something.getId()), newDocument).getMatchedCount();
    }


    @Override
    public long deleteSomething(String id) {
        DeleteResult deleteResult = this.somethingCollection.deleteOne(Filters.eq(SOMETHING_ID, id));
        return deleteResult.getDeletedCount();
    }


    @Override
    public List<Something> findSomethings(Integer start, Integer size) {

        FindIterable<Document> result = null;
        MongoCursor<Document> cursor = null;
        List<Something> toReturn = new LinkedList<>();
        try{
            result = this.somethingCollection.find();
            result.skip(start).limit(size).sort(new BasicDBObject("id", 1 ) );

            cursor =result.iterator();

            while (cursor.hasNext()){
                Document document = cursor.next();
                String somethingId = (String) document.get(SOMETHING_ID);
                String val1 = (String) document.get(SOMETHING_VAL1);
                String val2 = (String) document.get(SOMETHING_VAL2);

                toReturn.add(new Something(somethingId, val1, val2));
            }

        }finally {
            if(cursor != null){
                cursor.close();
            }
        }



        cursor.close();

        return toReturn;
    }


    @Override
    public boolean dropSomethingsCollection() {
        if(isExistCollectionName(COLLECTION_NAME)){
            this.mongoDatabase.getCollection(COLLECTION_NAME).drop();
            return true;
        }
        return  false;
    }

}
