package com.algo.homework.run;


import com.algo.homework.repo.SomethingRepository;
import com.algo.homework.repo.SomethingRepositoryImpl;
import com.algo.homework.repo.model.Something;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {


    public static  List<Something> generateSomethings(int id){
        Random random = new Random();

        List<Something> list = new LinkedList<>();

        for(int i = 0; i < 100; i++){
            Integer randomInt = random.nextInt(100000) + 100;
            String randomStringValue =  RandomStringUtils.randomAlphanumeric(20).toLowerCase();
            list.add(new Something(""+id, randomInt.toString(), randomStringValue));
            id++;
        }

        return list;
    }


    public static void main(String[] args){

        ApplicationContext context = new AnnotationConfigApplicationContext("com.algo.homework.repo.config");
        SomethingRepository somethingRepository = (SomethingRepository) context.getBean(SomethingRepositoryImpl.class);

        somethingRepository.dropSomethingsCollection();


        somethingRepository.createSomethingCollection();



        int id = 0;
        while( id < 10000){
            List<Something> somethings = generateSomethings(id);
            somethingRepository.insertAll(somethings);
            id += somethings.size();
            System.out.println("Current size  => id " + id);
        }


        /*long howManyDeleted = somethingRepository.deleteSomething("1");
        System.out.println("Removed => howManyDeleted " + howManyDeleted);

        Something found = somethingRepository.findSomething("0");
        System.out.println("FOUND  => " + found);
        found.setVal1("AAAAAAAAAA");
        found.setVal2("BBBBBBBB");

        long howManyEdited = somethingRepository.editSomething(found);
        System.out.println("Edit => howManyEdited " + howManyEdited);

        found = somethingRepository.findSomething("0");
        System.out.println("FOUND edited => " + found);

        List<Something> list = somethingRepository.findSomethings(0, 100);
        System.out.println("Paging => list " + list);

        list = somethingRepository.findSomethings(100, 100);
        System.out.println("Paging => list " + list);


        list = somethingRepository.findSomethings(200, 100);
        System.out.println("Paging => list " + list);
        */
    }

}
