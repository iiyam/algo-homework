package com.algo.homework.repo.service;

import com.algo.homework.api.v1_0.exceptions.ResourceNotFoundException;
import com.algo.homework.repo.SomethingRepository;
import com.algo.homework.repo.model.Something;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SomethingServiceTest {

    private SomethingRepositoryMock somethingRepositoryMock;
    private SomethingRepository somethingRepository;
    private SomethingMediator somethingService;

    @Before
    public void setUp() throws Exception {
        somethingRepositoryMock = new SomethingRepositoryMock();
        somethingRepository = somethingRepositoryMock;
        somethingService = new SomethingMediator();
        somethingService.setSomethingRepository(somethingRepository);
    }

    @After
    public void tearDown() throws Exception {
        this.somethingRepositoryMock = null;
        this.somethingRepository = null;
        this.somethingService = null;
    }


    public static final String SOMETHING_TO_FIND_BY_ID = "11";

    @Test
    public void testFindSomething_okIfFound() throws Exception {
        System.out.println("Test => testFindSomething_okIfFound" );
        Something something =  somethingService.findSomething(SOMETHING_TO_FIND_BY_ID);
        Assert.assertEquals(SomethingRepositoryMock.SOMETHING_FOR_ID_11.getVal1(), something.getVal1());
    }



    @Test(expected = ResourceNotFoundException.class)
    public void testFindSomething_notFound() throws Exception {
        System.out.println("Test => testFindSomething_notFound" );
        Something something =  somethingService.findSomething("10");
    }



    @Test
    public void testDeleteSomething_checkHowManyTimesInvokedMethod() throws Exception {
        System.out.println("Test => testDeleteSomething_checkHowManyTimesInvokedMethod" );
        this.somethingService.deleteSomething("11");
        int counter = this.somethingRepositoryMock.getCounterOfInvokedDeleteSomething();
        Assert.assertEquals((Integer)1, (Integer)counter);
    }
}