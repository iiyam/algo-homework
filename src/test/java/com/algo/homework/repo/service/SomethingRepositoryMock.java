package com.algo.homework.repo.service;


import com.algo.homework.repo.SomethingRepository;
import com.algo.homework.repo.model.Something;

import java.util.List;

public class SomethingRepositoryMock implements SomethingRepository {

    private int counterOfInvokedDeleteSomething = 0;
    public static final Something SOMETHING_FOR_ID_11 = new Something("11", "112233", "ffff");
    @Override
    public void init() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public boolean createSomethingCollection() {
        return false;
    }

    @Override
    public Integer insertAll(List<Something> list) {
        return null;
    }

    @Override
    public Something findSomething(String id) {
        if(id != null && id.equals("11")){
            return SOMETHING_FOR_ID_11;
        }
        return  null;
    }

    @Override
    public long editSomething(Something something) {
        return 0;
    }

    @Override
    public long deleteSomething(String id) {
        counterOfInvokedDeleteSomething++;
        return 1;
    }

    @Override
    public List<Something> findSomethings(Integer start, Integer size) {
        return null;
    }

    @Override
    public boolean dropSomethingsCollection() {
        return false;
    }


    public int getCounterOfInvokedDeleteSomething() {
        return counterOfInvokedDeleteSomething;
    }

    public void setCounterOfInvokedDeleteSomething(int counterOfInvokedDeleteSomething) {
        this.counterOfInvokedDeleteSomething = counterOfInvokedDeleteSomething;
    }
}
